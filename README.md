Reflex Streaming Helper
=======================

Reflex Streaming Helper web based application provides some HTML overlays for your streams during tournaments so that you can show match information like score or player names. You can use BrowserSource in OBS to show these overlays.

It is a Flask application with a basic HTML stack in front.

Installation
------------

`pip install reflex-straming-helper-0.3.0.tar.gz`

You need to create a few JSON files in your Flask instance directory, namely:

 - config.py
 - players.json
 - maps.json
 - tournament.json
 - current_match.json
 
### Sample config.py

    SECRET_KEY = 'YOUR_RANDOM_SECRET_KEY_HERE'
    STATIC_URL = 'http://localhost:9999/'
    BASIC_AUTH_USERNAME = 'admin'
    BASIC_AUTH_PASSWORD = '1234'
 
### Sample players.json

    [
      {
        "name": "Player Name 1",
        "country": "United Kingdom",
        "rank": "Diamond",
        "mmr": 2184,
        "best_mmr": 2196,
        "arena": "Ruin",
        "mouse": "Logitech Pro Gaming Mouse",
        "keyboard": "CoolerMaster Quickfire (Cherry MX Brown)",
        "monitor": "BenQ XL 2411",
        "twitch": "twitch_channel_name",
        "twitter": "twitter_handle",
        "discord": "discord_name"
      },
      {
        "name": "Player Name 2",
        "country": "Finland"
      }
    ]

### Sample maps.json

    [
      {
        "name" : "Furnace",
        "creator": "def",
        "thumbnail_images": ["Furnace1Medium.jpg", "Furnace2Medium.jpg", "Furnace3Medium.jpg", "Furnace4Medium.jpg"],
        "videos": [
          {
            "file_name": "FurnaceRed.mp4"
          },
          {
            "file_name": "FurnaceMega.mp4"
          },
          {
            "file_name": "FurnaceBolt.mp4"
          }
        ],
        "mega_health_count": 1,
        "hp50_count": 0,
        "hp25_count": 5,
        "hp5_count": 5,
        "red_armor_count": 1,
        "yellow_armor_count": 2,
        "green_armor_count": 0,
        "shard_count": 5,
        "bolt_rifle_count": 1,
        "ion_cannon_count": 1,
        "rocket_launcher_count": 2,
        "plasma_gun_count": 1,
        "grenade_launcher_count": 1,
        "shotgun_count": 1
      },
      {
        "name" : "Pocket Infinity",
        "creator": "tehace",
        "thumbnail_images": ["PocketInfinity1Medium.jpg", "PocketInfinity2Medium.jpg", "PocketInfinity3Medium.jpg", "PocketInfinity4Medium.jpg"],
        "videos": [
          {
            "file_name": "PocketInfinityRed.mp4"
          },
          {
            "file_name": "PocketInfinityMega.mp4"
          }
        ],
        "mega_health_count": 1,
        "hp50_count": 1,
        "hp25_count": 4,
        "hp5_count": 10,
        "red_armor_count": 1,
        "yellow_armor_count": 2,
        "green_armor_count": 1,
        "shard_count": 7,
        "bolt_rifle_count": 1,
        "ion_cannon_count": 1,
        "rocket_launcher_count": 2,
        "plasma_gun_count": 1,
        "grenade_launcher_count": 1,
        "shotgun_count": 1
      }
    ]

### Sample tournaments.json

    {
      "maps": [
        "Furnace",
        "Pocket Infinity",
        "Simplicity",
        "The Catalyst"
      ],
      "players": [
        "Player 1",
        "Player 2",
        "Player 3",
        "Player 4"
      ],
      "title": "RMC #EU4"
    }

### Sample current_match.json

    {
      "best_of": 3,
      "map": "Pocket Infinity",
      "match_started": true,
      "team_1_players": [
        "Player 1"
      ],
      "team_1_score": 1,
      "team_2_players": [
        "Player 2"
      ],
      "team_2_score": 2
    }


Usage
-----

Create a script like `examples/run.py` to create an instance and run.

Then you can go to:

 - __Admin panel__: http://localhost:9999/admin/
 - __Waiting animation__: http://localhost:9999/
 - __Current match info__: http://localhost:9999/current-match
 - __Current match overlay__: http://localhost:9999/current-match-overlay
 - __Furnace map info__: http://localhost:9999/map?name=Furnace
 - __The Catalyst map info__: http://localhost:9999/map?name=The Catalyst
 - __Pocket Infinity map info__: http://localhost:9999/map?name=Pocket Infinity
