from setuptools import setup, find_packages


setup(
    name='reflex-streaming-helper',
    version='0.4.3',
    description='A web app for common streaming scenes for tournaments.',
    author='Canol Gokel',
    packages=find_packages(exclude=['contrib', 'docs', 'tests', 'examples', 'instance']),
    install_requires=['Flask'],
    include_package_data=True,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Science/Research',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
)
