from reflex_streaming_helper import create_app

config_name = 'development'
app = create_app(config_name)

if __name__ == '__main__':
    app.run(threaded=True, host="0.0.0.0", port=9999)
