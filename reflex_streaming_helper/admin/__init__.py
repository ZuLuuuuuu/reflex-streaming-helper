from flask import Blueprint

admin = Blueprint('admin', __name__)

from reflex_streaming_helper.admin import views