import json
from functools import wraps

import flask
import os
from flask import abort, render_template, jsonify, request

from reflex_streaming_helper.admin import admin


def check_auth(entered_username, entered_password):
    username = flask.current_app.config['BASIC_AUTH_USERNAME']
    password = flask.current_app.config['BASIC_AUTH_PASSWORD']

    if username == entered_username.strip() and password == entered_password.strip():
        return True
    else:
        return False


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            abort(401)
        return f(*args, **kwargs)
    return decorated


@admin.route('/', methods=['GET'])
def get_admin():
    return render_template('admin.jinja2')


@admin.route('/authenticate', methods=['POST'])
def post_authenticate():
    if 'username' not in request.form or request.form['username'] == '' or 'password' not in request.form or request.form['password'].strip() == '':
        abort(400, 'You cannot leave username or password empty.')

    if check_auth(request.form['username'].strip(), request.form['password'].strip()):
        return "success"
    else:
        abort(500, 'Wrong username or password.')


@admin.route('/players', methods=['GET'])
@requires_auth
def get_players():
    players = load_instance_json('players.json')

    sorted_players = sorted(players, key=lambda p: p['name'].lower())

    return jsonify(sorted_players)


@admin.route('/maps', methods=['GET'])
@requires_auth
def get_maps():
    maps = load_instance_json('maps.json')

    sorted_maps = sorted(maps, key=lambda m: m['name'].lower())

    return jsonify(sorted_maps)


@admin.route('/tournament', methods=['GET'])
@requires_auth
def get_tournament():
    tournament = load_instance_json('tournament.json')

    sorted_players = sorted(tournament['players'], key=lambda p: p.lower())
    tournament['players'] = sorted_players

    sorted_maps = sorted(tournament['maps'], key=lambda m: m.lower())
    tournament['maps'] = sorted_maps

    return jsonify(tournament)


@admin.route('/tournament-players', methods=['GET'])
@requires_auth
def get_tournament_players():
    tournament = load_instance_json('tournament.json')

    tournament_players = sorted(tournament["players"], key=lambda s: s.lower())

    return jsonify(tournament_players)


@admin.route('/tournament-players', methods=['POST'])
@requires_auth
def post_tournament_players():
    tournament = load_instance_json('tournament.json')
    players = load_instance_json('players.json')

    player_name = request.data.decode('utf-8').strip()

    if player_name not in tournament['players']:
        tournament['players'].append(player_name)

    save_instance_json('tournament.json', tournament)

    return "success"


@admin.route('/tournament-players', methods=['DELETE'])
@requires_auth
def delete_tournament_players():
    tournament = load_instance_json('tournament.json')

    player_name = request.args.get('name').strip()

    if player_name in tournament['players']:
        index = tournament['players'].index(player_name)
        del tournament['players'][index]

    save_instance_json('tournament.json', tournament)

    return "success"


@admin.route('/tournament-maps', methods=['GET'])
@requires_auth
def get_tournament_maps():
    tournament = load_instance_json('tournament.json')

    tournament_maps = sorted(tournament["maps"], key=lambda s: s.lower())

    return jsonify(tournament_maps)


@admin.route('/tournament-maps', methods=['POST'])
@requires_auth
def post_tournament_maps():
    tournament = load_instance_json('tournament.json')
    maps = load_instance_json('maps.json')

    map_name = request.data.decode('utf-8').strip()

    if map_name not in tournament['maps']:
        tournament['maps'].append(map_name)

    save_instance_json('tournament.json', tournament)

    return "success"


@admin.route('/tournament-maps', methods=['DELETE'])
@requires_auth
def delete_tournament_maps():
    tournament = load_instance_json('tournament.json')

    map_name = request.args.get('name').strip()

    if map_name in tournament['maps']:
        index = tournament['maps'].index(map_name)
        del tournament['maps'][index]

    save_instance_json('tournament.json', tournament)

    return "success"


@admin.route('/tournament-title', methods=['POST'])
@requires_auth
def post_tournament_title():
    tournament = load_instance_json('tournament.json')

    title = request.data.decode('utf-8').strip()

    tournament['title'] = title

    save_instance_json('tournament.json', tournament)

    return "success"


@admin.route('/current-match', methods=['GET'])
@requires_auth
def get_current_match():
    current_match = load_instance_json('current_match.json')

    return jsonify(current_match)


@admin.route('/current-match', methods=['POST'])
@requires_auth
def post_current_match():
    current_match = load_instance_json('current_match.json')

    request_json = request.get_json()

    current_match['best_of'] = request_json['best_of']
    current_match['team_1_players'][0] = request_json['player_1']
    current_match['team_1_score'] = request_json['team_1_score']
    current_match['team_2_players'][0] = request_json['player_2']
    current_match['team_2_score'] = request_json['team_2_score']
    if 'winner' in request_json:
        current_match['winner'] = request_json['winner']
    if 'title' in request_json:
        current_match['title'] = request_json['title']

    save_instance_json('current_match.json', current_match)

    return "success"


def load_instance_json(file_name):
    json_file = flask.current_app.open_instance_resource(file_name)
    json_file_content = json_file.read().decode('utf-8')
    json_file.close()

    dict_json = json.loads(json_file_content)

    return dict_json


def save_instance_json(json_file_name, json_content):
    json_file_path = os.path.join(flask.current_app.instance_path, json_file_name)

    json_content_dumped = json.dumps(json_content, indent=4, sort_keys=True)

    with open(json_file_path, 'w') as json_file:
        json_file.write(json_content_dumped)


@admin.context_processor
def inject_static_url():
    static_url = flask.current_app.config['STATIC_URL']
    return dict(static_url=static_url)
