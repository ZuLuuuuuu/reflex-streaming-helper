import flask
from flask import render_template, request
import copy
import json

from reflex_streaming_helper.home import home


@home.route('/', methods=['GET'])
def get_intro():
    return render_template('intro.jinja2')


@home.route('/outro', methods=['GET'])
def get_outro():
    return render_template('outro.jinja2')


@home.route('/current-match-score', methods=['GET'])
def get_current_match_score():
    current_match = load_instance_json('current_match.json')

    return flask.jsonify(current_match)


@home.route('/current-match', methods=['GET'])
def get_current_match():
    return render_template('match.jinja2', **get_match_json("current_match.json"))


@home.route('/current-match-overlay', methods=['GET'])
def get_current_match_overlay():
    return render_template('match_overlay.jinja2', **get_match_json("current_match.json"))


@home.route('/next-match', methods=['GET'])
def get_next_match():
    return render_template('match.jinja2', **get_match_json("next_match.json"))


@home.route('/next-match-overlay', methods=['GET'])
def get_next_match_overlay():
    return render_template('match_overlay.jinja2', **get_match_json("next_match.json"))


@home.route('/current-match-winner', methods=['GET'])
def get_current_match_winner():
    return render_template('current_match_winner.jinja2', **get_match_json("current_match.json"))


def get_match_json(json_file):
    players_json = load_instance_json('players.json')
    current_match_json = load_instance_json(json_file)

    json_response = copy.copy(current_match_json)

    for player_index, player_name in enumerate(current_match_json["team_1_players"]):
        player_search_result = [player for player in players_json if player.get('name', '') == player_name]

        if len(player_search_result) > 0:
            json_response["team_1_players"][player_index] = player_search_result[0]
        else:
            del json_response["team_1_players"][player_index]

    for player_index, player_name in enumerate(current_match_json["team_2_players"]):
        player_search_result = [player for player in players_json if player.get('name', '') == player_name]

        if len(player_search_result) > 0:
            json_response["team_2_players"][player_index] = player_search_result[0]
        else:
            del json_response["team_2_players"][player_index]

    tournament_json = load_instance_json('tournament.json')

    json_response["tournament_title"] = tournament_json["title"]

    return json_response


@home.route('/tournament-intro', methods=['GET'])
def get_tournament_intro():
    tournament_json = load_instance_json('tournament.json')

    return render_template('tournament_intro.jinja2', **tournament_json)


@home.route('/map', methods=['GET'])
def get_map():
    map_name = request.args.get('name')

    maps_json = load_instance_json('maps.json')

    map_search_result = [map for map in maps_json if map.get('name', '') == map_name]

    tournament_json = load_instance_json('tournament.json')

    json_response = copy.copy(map_search_result[0])
    json_response["tournament_title"] = tournament_json["title"]

    return render_template('map.jinja2', **json_response)


@home.context_processor
def inject_static_url():
    static_url = flask.current_app.config['STATIC_URL']
    return dict(static_url=static_url)


def load_instance_json(file_name):
    json_file = flask.current_app.open_instance_resource(file_name)
    json_file_content = json_file.read().decode('utf-8')
    json_file.close()

    dict_json = json.loads(json_file_content)

    return dict_json
