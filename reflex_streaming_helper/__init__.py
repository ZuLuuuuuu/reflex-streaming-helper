from flask import Flask

from reflex_streaming_helper.config import app_config


def create_app(config_name):
    app = Flask(__name__, instance_relative_config=True)

    app.config.from_object(app_config[config_name])
    app.config.from_pyfile('config.py')

    from reflex_streaming_helper.admin import admin as admin_blueprint
    app.register_blueprint(admin_blueprint, url_prefix='/admin')

    from reflex_streaming_helper.home import home as home_blueprint
    app.register_blueprint(home_blueprint)

    return app
