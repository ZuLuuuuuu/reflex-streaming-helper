var app = new Vue({
  el: '#app',
  data: function () {
    return {
      basicAuthUsername: '',
      basicAuthPassword: '',
      authenticated: false,
      players: [],
      maps: [],
      selectedPlayer: null,
      selectedMap: null,
      tournamentTitle: '',
      tournamentPlayers: [],
      tournamentMaps: [],
      currentMatchBestOf: 1,
      currentMatchPlayer1: null,
      currentMatchTeam1Score: 0,
      currentMatchPlayer2: null,
      currentMatchTeam2Score: 0,
      currentMatchTitle: '',
      currentMatchWinner: null
    }
  },
  watch: {
    tournamentTitle: function(newValue) {
      let headers = {Authorization: this.getAuthorizationString()};

      this.$http.post('/admin/tournament-title', this.tournamentTitle, {headers: headers}).then(function(response) {
      }, function(response) {
        // error callback
      });
    },
    currentMatchBestOf: function() {
      this.postCurrentMatch();
    },
    currentMatchPlayer1: function() {
      this.postCurrentMatch();
    },
    currentMatchTeam1Score: function() {
      this.postCurrentMatch();
    },
    currentMatchPlayer2: function() {
      this.postCurrentMatch();
    },
    currentMatchTeam2Score: function() {
      this.postCurrentMatch();
    },
    currentMatchWinner: function() {
      this.postCurrentMatch();
    },
    currentMatchTitle: function() {
      this.postCurrentMatch();
    }
  },
  methods: {
    getAuthorizationString() {
      return 'Basic ' + btoa(this.basicAuthUsername + ':' + this.basicAuthPassword);
    },
    login: function() {
      var formData = new FormData();
      formData.append('username', this.basicAuthUsername);
      formData.append('password', this.basicAuthPassword);

      this.$http.post('/admin/authenticate', formData).then(function(response) {
        this.authenticated = true;
        this.refresh();
      }, function(response) {
        this.authenticated = false;
        alert('Bad password');
      });
    },
    loadPlayers: function() {
      let headers = {Authorization: this.getAuthorizationString()};

      this.$http.get('/admin/players', {headers: headers}).then(function(response) {
        this.players = response.body;
        this.selectedPlayer = this.players[0].name;
      }, function(response) {
        // error callback
      });
    },
    loadMaps: function() {
      let headers = {Authorization: this.getAuthorizationString()};

      this.$http.get('/admin/maps', {headers: headers}).then(function(response) {
        this.maps = response.body;
        this.selectedMap = this.maps[0].name;
      }, function(response) {
        // error callback
      });
    },
    loadTournament: function() {
      let headers = {Authorization: this.getAuthorizationString()};

      this.$http.get('/admin/tournament', {headers: headers}).then(function(response) {
        this.tournamentTitle = response.body.title;
        this.tournamentPlayers = response.body.players;
        this.tournamentMaps = response.body.maps;
      }, function(response) {
        // error callback
      });
    },
    loadTournamentPlayers: function() {
      let headers = {Authorization: this.getAuthorizationString()};

      this.$http.get('/admin/tournament-players', {headers: headers}).then(function(response) {
        this.tournamentPlayers = response.body;
      }, function(response) {
        // error callback
      });
    },
    loadTournamentMaps: function() {
      let headers = {Authorization: this.getAuthorizationString()};

      this.$http.get('/admin/tournament-maps', {headers: headers}).then(function(response) {
        this.tournamentMaps = response.body;
      }, function(response) {
        // error callback
      });
    },
    postTournamentPlayer: function() {
      let headers = {Authorization: this.getAuthorizationString()};

      this.$http.post('/admin/tournament-players', this.selectedPlayer, {headers: headers}).then(function(response) {
        this.loadTournamentPlayers();
      }, function(response) {
        // error callback
      });
    },
    deleteTournamentPlayer: function(playerName) {
      let headers = {Authorization: this.getAuthorizationString()};

      this.$http.delete('/admin/tournament-players', {params: {name: playerName}, headers: headers}).then(function(response) {
        this.loadTournamentPlayers();
      }, function(response) {
        // error callback
      });
    },
    postTournamentMap: function() {
      let headers = {Authorization: this.getAuthorizationString()};

      this.$http.post('/admin/tournament-maps', this.selectedMap, {headers: headers}).then(function(response) {
        this.loadTournamentMaps();
      }, function(response) {
        // error callback
      });
    },
    deleteTournamentMap: function(mapName) {
      let headers = {Authorization: this.getAuthorizationString()};

      this.$http.delete('/admin/tournament-maps', {params: {name: mapName}, headers: headers}).then(function(response) {
        this.loadTournamentMaps();
      }, function(response) {
        // error callback
      });
    },
    loadCurrentMatch: function() {
      let headers = {Authorization: this.getAuthorizationString()};

      this.$http.get('/admin/current-match', {headers: headers}).then(function(response) {
        this.currentMatchBestOf = response.body.best_of;
        this.currentMatchPlayer1 = response.body.team_1_players[0];
        this.currentMatchTeam1Score = response.body.team_1_score;
        this.currentMatchPlayer2 = response.body.team_2_players[0];
        this.currentMatchTeam2Score = response.body.team_2_score;
      }, function(response) {
        // error callback
      });
    },
    postCurrentMatch: function() {
      var currentMatch = {
        "best_of": this.currentMatchBestOf,
        "player_1": this.currentMatchPlayer1,
        "team_1_score": this.currentMatchTeam1Score,
        "player_2": this.currentMatchPlayer2,
        "team_2_score": this.currentMatchTeam2Score,
        "title": this.currentMatchTitle,
        "winner": this.currentMatchWinner
      };

      let headers = {Authorization: this.getAuthorizationString()};

      this.$http.post('/admin/current-match', currentMatch, {headers: headers}).then(function(response) {
      }, function(response) {
        // error callback
      });
    },
    refresh: function() {
      this.loadPlayers();
      this.loadMaps();
      this.loadTournament();
      this.loadCurrentMatch();
    }
  },
  mounted: function() {
    this.refresh();
  }
});
